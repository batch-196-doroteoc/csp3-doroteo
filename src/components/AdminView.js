import { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import AddProduct from './AddProduct';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';


export default function AdminView(props) {

	console.log(props)
	const { productsData, fetchData } = props;

	const [ products, setproducts ] = useState([])


	useEffect(() => {

		const productsArr = productsData.map(product => {
			return(
				<tr key={product._id}>
					<td>{product._id}</td>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td className={product.isActive ? "text-success" : "text-danger"}>
						{product.isActive ? "Available" : "Unavailable"}
					</td>
					<td>
						<EditProduct product={product._id} fetchData={fetchData}/>
					</td>
					<td>
						<ArchiveProduct product={product._id} isActive={product.isActive} fetchData={fetchData}/>
					</td>
				</tr>
				)
		})
		setproducts(productsArr)
	}, [productsData])


	return(
		<>
			<div className="text-center my-4">
				<h1>Welcome, Admin!</h1>
				<AddProduct fetchData={fetchData} />
			</div>
			
			<Table className="colorPurple">
				<thead className="text-white">
					<tr>
						<th>Product ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Active</th>
						<th Colspan="2">Update/Enable/Disable</th>
					</tr>
				</thead>

				<tbody>
					{ products }
				</tbody>
			</Table>

		</>

		)
}
