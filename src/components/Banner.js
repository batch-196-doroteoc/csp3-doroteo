import { Link } from 'react-router-dom'
import {Button, Row, Col} from 'react-bootstrap';
import Gallery from '../components/Gallery';

export default function Banner({data}){

	console.log(data)
    const {title, content, destination1, label1,destination2,label2,destination3,label3,destination4,label4, image} = data;

	return (
		<Row>
				<Col className = "p-5">
				<Gallery/>
				</Col>

				<Col className = "p-5">
				<Row>
				<h1>{title}</h1>
				<p>{content}</p>
				</Row>
				<Row className = "pb-3">
				<Button div className="outline-dark text-wrap me-2" style={{width: "8rem"}} variant="outline-dark" as= {Link} to={destination1}>{label1}</Button>
				<Button div className="outline-dark text-wrap me-2" style={{width: "8rem"}} variant="outline-dark" as= {Link} to={destination3}>{label3}</Button>
				</Row>
				<Row>
				<Button div className="outline-dark text-wrap me-2" style={{width: "8rem"}} variant="outline-dark" as= {Link} to={destination2}>{label2}</Button>
				<Button div className="outline-dark text-wrap me-2" style={{width: "8rem"}} variant="outline-dark" as= {Link} to={destination4}>{label4}</Button>
				</Row>
				</Col>

				<Col className = "p-5">
				<p><img alt="PopShop!" src={image}/></p>
				</Col>

				

				
		</Row>
	)
}
