import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
// import AppNavbar from './components/AppNavbar';
//comment out const root
//uncomment this out again after making a new variable, refactor,kasi may iiinstall
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
{/*  <AppNavbar/>*/}
    <App />
  </React.StrictMode>
);

//we are going to make a new variable
// const name = "Tony Stark";
// const element = <h1> Hello, {name}</h1>
// const user = {
//   firstName: 'Mikasa',
//   lastName: 'Ackerman',
// };

// const formatName = (user) => {
//   return user.firstName + ' ' + user.lastName;
// }

// const fullName = <h1>Hello, {formatName(user)}!</h1>
// //we cannot put two variables for now
// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(fullName);

//before react 18
/*
  ReactDOM.render(
  element,
  document.getElementById('root')
  );
  

*/