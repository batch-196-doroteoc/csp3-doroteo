import Banner from '../components/Banner';

export default function Error() {

    const data = {
        title: "Error! Under Maintenance!",
        content: "The page you are looking for cannot be found",
        destination1: "/",
        label1: "Back home",
        destination2: "/products",
        label2:"Buy Now!",
        destination3: "/register",
        label3: "Not yet registered?",
        destination4: "/login",
        label4:"Log in Here!",
        image: "./img/3.png"
    }
    
    return (
        <Banner data={data}/>
    )
}
