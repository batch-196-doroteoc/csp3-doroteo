import Banner from '../components/Banner';
import Highlights from '../components/Highlights';



export default function Home() {
	const data = {
        title: "PopShop!",
        content: "Shop More, Shop Now",
        destination1: "/products",
        label1: "Buy now and get discounts!",
        destination2: "/profile",
        label2:"View your account",
        destination3: "/register",
        label3: "Not yet registered?",
        destination4: "/login",
        label4:"Log in Here!",
        image: "./img/3.png"
    }

    return (
        <>
	        <Banner data={data}/>
	        <Highlights />
		</>

    )
}
