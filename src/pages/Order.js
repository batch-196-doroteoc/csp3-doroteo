import {useState, useEffect, useContext} from 'react';
import {useParams, Navigate } from 'react-router-dom';
import OrderCard from '../components/OrderCard';
import UserContext from '../UserContext';


export default function OrderView () {

	const {user} = useContext(UserContext);
	const token = localStorage.getItem("token")
	// const [orderId,setOrderId] = useState("");
	// const [totalAmount, setTotalAmount] = useState(0);
	// const [purchasedOn, setPurchasedOn] = useState("");
	const [orders, setOrders] = useState([])
	
	useEffect(()=>{
		fetch("http://localhost:4000/orders/getUserOrders",{
			method: "GET",
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
			// setOrderId(data.orderId);
			// setTotalAmount(data.total);
			// setPurchasedOn(data.purchasedOn);
			setOrders(data.map(order=>{
				return(
					<OrderCard key={order._id} productProp = {order}/>
				)
			}))

		})

	},[])
	
	return(
		<>
			<h1>User Orders:</h1>
		{/*	{orders}*/}
		</>
		)
}