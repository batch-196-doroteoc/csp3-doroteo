import {useState, useEffect} from 'react'
import {Container, Card, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom';
import Order from '../pages/Order';


export default function Profile() {

    const [userDetails, setUserDetails] = useState({
        firstName: null,
        lastName: null,
        email: null,
        mobileNo: null,
        isAdmin: null,
        _id: null

    })

    useEffect(()=>{
    fetch('http://localhost:4000/users/getUserDetails',{
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data=> {
      console.log(data);
      setUserDetails({
        _id: data._id,
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email,
        mobileNo: data.mobileNo,
        isAdmin: data.isAdmin
      })
    })
    },[])

    return(
        <div
            className="profileBg"
            style={{height: 500}}
        >
        <Container className="pt-5">
        <Card >
              <Card.Header as="h5">Profile</Card.Header>
              <Card.Body className="colorPurple">
                <Card.Title className="mb-3">User Details</Card.Title>
                <Card.Text>
                  First Name: {userDetails.firstName} 
                </Card.Text>
                <Card.Text>
                  Last Name: {userDetails.lastName} 
                </Card.Text>
                <Card.Text>
                  Email: {userDetails.email}  
                </Card.Text>
                 <Card.Text>
                  MobileNo: {userDetails.mobileNo}  
                </Card.Text>
              </Card.Body>
                    {
                   (userDetails.isAdmin === false) ?
                   <>
                    <div className="text-center">
                    <Link className="btn btn-outline-dark text-wrap mt=2" to={`/orders/getUserOrders`}>Order History</Link>
                    </div> 
                   </>
                   :
                   <>
                    <div className="text-center">
                    <Link className="btn btn-outline-dark text-wrap mt=2" to={`/products/create`}>Add Product</Link>
                    </div>
                   </>}
            </Card>
         </Container>
        
        </div>
    )
}

